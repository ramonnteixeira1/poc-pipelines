import Vue from 'vue'
import Router from 'vue-router'
import Board from '@/views/Board.vue'
import Register from '@/views/Register.vue'
import CustomizeStyle from '@/views/CustomizeStyle.vue'
import ExportKudo from '@/views/ExportKudo.vue'
import Lottery from '@/views/Lottery.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'board',
      component: Board,
      meta: {
        requireAuth: true
      }
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      meta: {
        requireAuth: true
        // requireRoles: [
        //   'TheMan',
        //   'Admin'
        // ]
      }
    },
    {
      path: '/customize',
      name: 'customize',
      component: CustomizeStyle,
      meta: {
        requireAuth: true
      }
    },
    {
      path: '/export',
      name: 'export',
      component: ExportKudo
    },
    {
      path: '/lottery',
      name: 'lottery',
      component: Lottery
    }
  ]
})
