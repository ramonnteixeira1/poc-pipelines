# Kudomundo Frontend


Projeto front feito com VueJS para a exibição do board e cadastro de Kudos.

Ele é apenas a casca do projeto, as customizações ficam no projeto kudomundo-whitelabel.

## Executando o projeto

Para instalar as dependências execute

```
yarn
```

Para executar o projeto

```
yarn start
```

Para executar o lint

```
yarn lint
```

Para verificar os testes

```
yarn test
```


## Andamento do projeto

1. [x] Definir layout padrão

2. [x] Definir variáveis de customização

3. [x] Adicionar autenticação OAuth2

4. [x] Separar front em componentes

5. [ ] Criar cadastro de time

    5.1 [ ] customização (logo, cores)

    5.2 [ ] Cadastro de membros

    5.3 [ ] Cadastro de figurinhas
