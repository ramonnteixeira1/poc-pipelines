import { shallowMount } from '@vue/test-utils'
import Component from '@/components/Modal.vue'

describe('Modal.vue', () => {
  it('renders Modal', () => {
    const wrapper = shallowMount(Component)
    expect(wrapper.name()).toBe('Modal')
    expect(wrapper.text()).toBeTruthy()
  })
})
