import { shallowMount } from '@vue/test-utils'
import Component from '@/components/StyleComponent.vue'
import flushPromises from 'flush-promises'

describe('StyleComponent.vue', () => {
  const propsData = {
    property: '--header-color',
    header: 'Cor do header da página'
  }

  const documentMock = {
    style: {
      __value: {},
      getPropertyValue (name) {
        return this.__value[name]
      },
      setProperty (name, value) {
        this.__value[name] = value
      }
    }
  }

  beforeEach(() => {
    Object.defineProperty(global.document, 'documentElement', { writable: true, value: documentMock })
    Object.defineProperty(global, 'getComputedStyle', { value: (documentElement) => documentElement.style })
  })

  it('renders component object when passed', () => {
    const wrapper = shallowMount(Component, { propsData })
    expect(wrapper.name()).toBe('StyleComponent')
  })

  it('should be change root style when change value', async () => {
    documentMock.style.setProperty('--header-color', 'blue')
    const wrapper = shallowMount(Component, { propsData, attachToDocument: true })

    await flushPromises()
    expect(wrapper.vm.$data.value).toBe('blue')

    wrapper.vm.$data.value = 'black'
    wrapper.vm.change()
    await flushPromises()

    expect(documentMock.style.getPropertyValue('--header-color')).toBe('black')
  })
})
