import { shallowMount } from '@vue/test-utils'
import components from '@/components/Card.vue'
import flushPromises from 'flush-promises'

describe('Card.vue', () => {
  it('renders card object when passed', async () => {
    const wrapper = shallowMount(components, {
      propsData: {
        card: {
          title: 'teste',
          member: 'fulano',
          description: 'top demais'
        }
      },
      mocks: {
        $html2canvas: jest.fn()
      }
    })

    await flushPromises()

    expect(wrapper.name()).toBe('Card')
    expect(wrapper.text()).toBeTruthy()
    expect(wrapper.vm.$data.team).toBe('bebulls')
    // expect(wrapper.vm.$html2canvas).toHaveBeenCalled()
  })
})
