import { shallowMount } from '@vue/test-utils'
import Component from '@/layouts/RegisterLayout.vue'
import VueRouter from 'vue-router'

describe('RegisterLayout.vue', () => {
  it('renders component when fetch members', async () => {
    const router = new VueRouter()
    const wrapper = shallowMount(Component, { router })

    expect(wrapper.name()).toBe('RegisterLayout')
    expect(wrapper.text()).toBeTruthy()
  })
})
