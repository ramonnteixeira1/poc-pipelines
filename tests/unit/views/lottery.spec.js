import { shallowMount, createLocalVue } from '@vue/test-utils'
import axios from 'axios'
import flushPromises from 'flush-promises'
import Component from '@/views/Lottery.vue'
import VueToastr from 'vue-toastr'

jest.mock('axios')

const localVue = createLocalVue()
localVue.use(VueToastr)

describe('Lottery.vue', () => {
  let wrapper

  const data = [{
    title: 'teste01',
    member: 'membro card 01',
    description: 'teste',
    timestamp: 1571172957
  },
  {
    title: 'teste02',
    member: 'membro card 02',
    description: 'teste',
    timestamp: 1571173278
  }]

  beforeEach(() => {
    const resp = { data }
    axios.get.mockImplementation((uri) => {
      if (uri.startsWith('http://localhost:3000/filter/bebulls')) {
        return Promise.resolve(resp)
      }
      return Promise.reject(Error('uri nao existe'))
    })

    wrapper = shallowMount(Component, { localVue })
  })

  it('renders component when axios get', () => {
    expect(wrapper.name()).toBe('Lottery')
  })

  it('should be make a winner when pick has been called', () => {
    global.setTimeout = (callback) => callback()
    wrapper.vm.$data.cards = data
    wrapper.vm.$data.running = true
    wrapper.vm.pick()
    expect(wrapper.vm.$data.running).toBeFalsy()
    expect(wrapper.vm.$data.winner).toBeTruthy()
  })

  it('should be call setTimeout when load success', async () => {
    const timeoutSpy = jest.spyOn(global, 'setTimeout')
    wrapper.vm.$data.running = false
    wrapper.vm.run()
    expect(wrapper.vm.$data.running).toBeTruthy()
    await flushPromises()
    expect(timeoutSpy).toBeCalled()
  })

  it('should be alert when load error', async () => {
    axios.get.mockImplementation((uri) => {
      if (uri.startsWith('http://localhost:3000/filter/bebulls')) {
        return Promise.reject(Error('uri nao existe'))
      }
      return Promise.resolve({ data })
    })

    const toastrSpy = jest.spyOn(wrapper.vm.$toastr, 'e')
    const pauseSpy = jest.fn()

    wrapper.vm.$data.audio = { pause: pauseSpy }
    wrapper.vm.$data.running = false
    wrapper.vm.run()
    expect(wrapper.vm.$data.running).toBeTruthy()
    await flushPromises()
    expect(pauseSpy).toBeCalled()
    expect(toastrSpy).toBeCalledWith('Deu um probleminha, tente mais tarde!')
  })

  it('should call audio.play', () => {
    const spy = jest.spyOn(wrapper.vm.audio, 'play')
    wrapper.vm.play()
    expect(wrapper.vm.$data.picked).toBeTruthy()
    expect(spy).toBeCalled()
  })
})
