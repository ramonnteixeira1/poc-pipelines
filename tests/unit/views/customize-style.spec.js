import { shallowMount, createLocalVue } from '@vue/test-utils'
import Component from '@/views/CustomizeStyle.vue'
import flushPromises from 'flush-promises'
import VueRouter from 'vue-router'
import VueToastr from 'vue-toastr'
import axios from 'axios'

jest.mock('axios')

describe('CustomizeStyle.vue', () => {
  const localVue = createLocalVue()
  localVue.use(VueRouter)
  localVue.use(VueToastr)
  const router = new VueRouter()

  const mount = () => {
    return shallowMount(Component, {
      localVue,
      router
    })
  }

  const documentMock = {
    style: {
      __value: {},
      getPropertyValue (name) {
        return this.__value[name]
      },
      setProperty (name, value) {
        this.__value[name] = value
      }
    }
  }

  const consoleLog = {
    value: '',
    log (message) {
      this.value = message
    }
  }

  beforeEach(() => {
    Object.defineProperty(global.document, 'documentElement', { writable: true, value: documentMock })
    Object.defineProperty(global, 'getComputedStyle', { value: (documentElement) => documentElement.style })
    Object.defineProperty(console, 'log', { value: (message) => consoleLog.log(message) })
  })

  it('renders component object when passed', () => {
    const wrapper = mount()
    expect(wrapper.name()).toBe('CustomizeStyle')
  })

  it('should be mount css when save', async () => {
    const expected =
    `:root {
    --header-color: undefined;
    --font-color: undefined;
    --button-color: undefined;
    --button-font: undefined;
    --filter-button-color: undefined;
    --header-bg-size: undefined;
    --header-bg-position-x: undefined;
    --header-bg-position-y: undefined;
    --form-title: undefined;
    --body-background-color: undefined;
    --body-background-size: undefined;
    --body-background-position-x: undefined;
    --body-background-position-y: undefined;
    --breadcrumb-dot-color: undefined;
    --breadcrumb-line-color: undefined;
    --color-1: undefined;
    --color-2: undefined;
    --color-3: undefined;
    --color-4: undefined;
}`
    axios.put.mockImplementation((uri, body) => {
      if (uri === 'http://localhost:3000/customize/bebulls/style') {
        consoleLog.log(body)
        return Promise.resolve([])
      }
      return Promise.reject(Error('teste'))
    })

    const wrapper = mount()
    const spy = jest.spyOn(wrapper.vm.$toastr, 's')
    wrapper.vm.$router.push = jest.fn()
    await flushPromises()
    wrapper.vm.save()
    await flushPromises()

    expect(consoleLog.value).toBe(expected)
    expect(wrapper.vm.$router.push).toBeCalledWith('/')
    expect(spy).toBeCalledWith('Customização feita com sucesso', 'Salvou com sucesso!')
  })

  it('should be toastr.e when axios fail', async () => {
    axios.put.mockImplementation((uri) => {
      if (uri === 'http://localhost:3000/customize/bebulls/style') {
        return Promise.reject(Error('teste'))
      }
      return Promise.resolve([])
    })

    const wrapper = mount()
    const spy = jest.spyOn(wrapper.vm.$toastr, 'e')
    await flushPromises()
    wrapper.vm.save()
    await flushPromises()

    expect(consoleLog.value).toBe('teste')
    expect(spy).toBeCalledWith('Não consegui salvar esta customização', 'Vish, deu ruim')
  })
})
